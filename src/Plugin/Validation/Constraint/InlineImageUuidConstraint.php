<?php

declare(strict_types=1);

namespace Drupal\inline_image_saver\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * Provides an InlineImageUuid constraint.
 */
#[Constraint(
  id: 'InlineImageUuid',
  label: new TranslatableMarkup('Inline image UUID', ['context' => 'Validation']),
)]
class InlineImageUuidConstraint extends SymfonyConstraint {

  /**
   * The default violation message.
   */
  public string $errorMessage = 'The image "@basename" was not loaded correctly. Please re-upload the image.';

}
