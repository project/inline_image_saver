<?php

declare(strict_types=1);

namespace Drupal\inline_image_saver;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\file\FileInterface;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;

/**
 * Provides an interface for the inline image savers.
 */
interface InlineImageSaverInterface {

  /**
   * The default upload directory name.
   */
  public const DEFAULT_DIR_NAME = 'inline-image-saver';

  /**
   * Downloads inline images for an entity if necessary and updates the markup.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param bool $remove_broken
   *   Whether to remove broken images.
   * @param string $broken_text
   *   The text to replace broken images with.
   *
   * @return bool
   *   TRUE if the entity was changed.
   */
  public function fixEntity(FieldableEntityInterface $entity, bool $remove_broken = FALSE, string $broken_text = ''): bool;

  /**
   * Checks if the inline image saver is enabled for an entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE if the inline image saver is enabled for the entity.
   */
  public function needEntityFix(FieldableEntityInterface $entity): bool;

  /**
   * Downloads inline images for an item if necessary and updates the markup.
   *
   * @param \Drupal\text\Plugin\Field\FieldType\TextItemBase $item
   *   The text item.
   * @param bool $remove_broken
   *   Whether to remove broken images.
   * @param string $broken_text
   *   The text to replace broken images with.
   *
   * @return bool
   *   TRUE if the item was changed.
   */
  public function fixItem(TextItemBase $item, bool $remove_broken = FALSE, string $broken_text = ''): bool;

  /**
   * Checks if an image is a valid file entity.
   *
   * @param \DOMElement $img
   *   The image element.
   *
   * @return bool
   *   TRUE if the image is a valid file entity.
   */
  public function isValidInlineImage(\DOMElement $img): bool;

  /**
   * Downloads an inline image for a text item.
   */
  public function downloadInlineImage(TextItemBase $item, string $src, bool $to_temp = FALSE): ?FileInterface;

  /**
   * Returns the text value of a text item if it contains inline images.
   */
  public function getTextItemDom(TextItemBase $item): ?\DOMDocument;

  /**
   * Sets the text value of a text item.
   */
  public function setTextItemDom(TextItemBase $item, \DOMDocument $value): void;

}
