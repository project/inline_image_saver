<?php

declare(strict_types=1);

namespace Drupal\inline_image_saver\Plugin\Validation\Constraint;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\inline_image_saver\Form\InlineImageSaverSettingsForm;
use Drupal\inline_image_saver\InlineImageSaverInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the InlineImageUuid constraint.
 */
class InlineImageUuidConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  use AutowireTrait;

  /**
   * Constructs the object.
   *
   * @param \Drupal\inline_image_saver\InlineImageSaverInterface $inlineImageSaver
   *   The inline image saver.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   */
  public function __construct(
    protected readonly InlineImageSaverInterface $inlineImageSaver,
    protected readonly ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * Checks if the passed value is valid.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $value
   *   The field item list.
   * @param \Drupal\inline_image_saver\Plugin\Validation\Constraint\InlineImageUuidConstraint $constraint
   *   The constraint.
   */
  public function validate($value, Constraint $constraint): void {
    $config = $this->configFactory->get(InlineImageSaverSettingsForm::CONFIG_NAME);
    $check_downloadable = $config->get('try_download') && $config->get('allow_if_downloadable');

    /** @var \Drupal\text\Plugin\Field\FieldType\TextItemBase $item */
    foreach ($value as $delta => $item) {
      if ($dom = $this->inlineImageSaver->getTextItemDom($item)) {
        /** @var \DOMElement $img */
        foreach ($dom->getElementsByTagName('img') as $img) {
          if ($this->inlineImageSaver->isValidInlineImage($img)) {
            continue;
          }

          $src = $img->getAttribute('src');
          if ($check_downloadable && $file = $this->inlineImageSaver->downloadInlineImage($item, $src, TRUE)) {
            try {
              // Try to remove the temporary file, if possible.
              $file->delete();
            }
            catch (\Throwable) {
              // Ignore.
            }
          }
          else {
            $this->context
              ->buildViolation($constraint->errorMessage, ['@basename' => basename($src)])
              ->atPath($delta)
              ->addViolation();
          }
        }
      }
    }
  }

}
