<?php

declare(strict_types=1);

namespace Drupal\inline_image_saver\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\SynchronizableInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\RedundantEditableConfigNamesTrait;

/**
 * Configures inline_image_saver.settings for this site.
 */
class InlineImageSaverSettingsForm extends ConfigFormBase {

  use AutowireTrait;
  use RedundantEditableConfigNamesTrait;

  /**
   * The module config name.
   */
  public const CONFIG_NAME = 'inline_image_saver.settings';

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'inline_image_saver_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable validation'),
      '#config_target' => static::CONFIG_NAME . ':validation',
    ];
    $form['try_download'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Try to download missing images'),
      '#config_target' => static::CONFIG_NAME . ':try_download',
    ];
    $form['skip_sync'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip download attempt when syncing'),
      '#config_target' => static::CONFIG_NAME . ':skip_sync',
      '#description' => $this->t('This will avoid slowing down the entity saving when needed by setting the sync flag. This works for entities that implement the @interface interface.', [
        '@interface' => SynchronizableInterface::class,
      ]),
      '#states' => [
        'visible' => [
          ':input[name="try_download"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['allow_if_downloadable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip validation if image is downloadable'),
      '#config_target' => static::CONFIG_NAME . ':allow_if_downloadable',
      '#states' => [
        'visible' => [
          ':input[name="validation"]' => ['checked' => TRUE],
          ':input[name="try_download"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['check_file_exists'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check if file exists'),
      '#config_target' => static::CONFIG_NAME . ':check_file_exists',
      '#description' => $this->t('By default, only entity existence is checked by <code>data-entity-*</code> attributes, this option tightens the validation by loading the file entity and checking if the file exists in the file system. This may be slow on slow file systems.'),
      '#states' => [
        'visible' => [
          ':input[name="validation"]' => ['checked' => TRUE],
          // @phpcs:ignore Squiz.Arrays.ArrayDeclaration.NoKeySpecified
          'or',
          ':input[name="try_download"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['ignore_blob'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore blob images'),
      '#config_target' => static::CONFIG_NAME . ':ignore_blob',
      '#description' => $this->t('Skip validating and downloading blob images.'),
      '#states' => [
        'visible' => [
          ':input[name="validation"]' => ['checked' => TRUE],
          // @phpcs:ignore Squiz.Arrays.ArrayDeclaration.NoKeySpecified
          'or',
          ':input[name="try_download"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['new_revision'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new revision'),
      '#config_target' => static::CONFIG_NAME . ':new_revision',
      '#states' => [
        'visible' => [
          ':input[name="try_download"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['revision_log'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Revision log'),
      '#config_target' => static::CONFIG_NAME . ':revision_log',
      '#states' => [
        'visible' => [
          ':input[name="new_revision"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $this->entityFieldManager->clearCachedFieldDefinitions();
  }

}
