<?php

declare(strict_types=1);

namespace Drupal\inline_image_saver;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\SynchronizableInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\inline_image_saver\Form\InlineImageSaverSettingsForm;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\MimeTypes;

/**
 * Service for organizing inline images.
 */
class InlineImageSaver implements InlineImageSaverInterface {

  /**
   * The default upload directory.
   */
  protected string $defaultUploadDir;

  /**
   * The upload directories cache keyed by the editor ID.
   */
  protected array $uploadDirs = [];

  /**
   * The module config.
   */
  protected ImmutableConfig $config;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\file\FileRepositoryInterface $fileRepository
   *   The file repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator.
   * @param \Drupal\Core\Routing\RequestContext $requestContext
   *   The request context.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly FileSystemInterface $fileSystem,
    protected readonly FileRepositoryInterface $fileRepository,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly ClientInterface $httpClient,
    protected readonly FileUrlGeneratorInterface $fileUrlGenerator,
    protected readonly RequestContext $requestContext,
    protected readonly AccountInterface $currentUser,
    protected readonly TimeInterface $time,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function fixEntity(FieldableEntityInterface $entity, bool $remove_broken = FALSE, string $broken_text = ''): bool {
    $changed = FALSE;
    foreach ($entity->getFieldDefinitions() as $definition) {
      if (is_subclass_of($definition->getItemDefinition()->getClass(), TextItemBase::class)) {
        /** @var \Drupal\text\Plugin\Field\FieldType\TextItemBase $item */
        foreach ($entity->get($definition->getName()) as $item) {
          if ($this->fixItem($item, $remove_broken, $broken_text)) {
            $changed = TRUE;
          }
        }
      }
    }
    if ($changed && $entity instanceof RevisionableInterface) {
      $config = $this->getConfig();
      if ($config->get('new_revision')) {
        // If this is already a new revision,
        // try to only set the messages if there is none,
        // so as not to overwrite the current revision data.
        if ($entity->isNewRevision()) {
          if ($entity instanceof RevisionLogInterface && !$entity->getRevisionLogMessage()) {
            $entity->setRevisionLogMessage($config->get('revision_log'));
          }
        }
        // Otherwise, create a new revision.
        else {
          $entity->setNewRevision();
          if ($entity instanceof RevisionLogInterface) {
            $entity->setRevisionUserId($this->currentUser->id())
              ->setRevisionCreationTime($this->time->getRequestTime())
              ->setRevisionLogMessage($config->get('revision_log'));
          }
        }
      }
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function needEntityFix(FieldableEntityInterface $entity): bool {
    $config = $this->getConfig();
    if (!$config->get('try_download')) {
      return FALSE;
    }
    if ($config->get('skip_sync') && $entity instanceof SynchronizableInterface && $entity->isSyncing()) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function fixItem(TextItemBase $item, bool $remove_broken = FALSE, string $broken_text = ''): bool {
    // Skip empty values.
    if (!$dom = $this->getTextItemDom($item)) {
      return FALSE;
    }
    $changed = FALSE;
    /** @var \DOMElement $img */
    foreach ($dom->getElementsByTagName('img') as $img) {
      // @todo maybe extract all UUIDs and check them all at once to reduce the number of database queries?
      if ($this->isValidInlineImage($img)) {
        continue;
      }
      // Replace the image with the downloaded image if it's downloaded.
      $src = $img->getAttribute('src');
      if ($src && $file = $this->downloadInlineImage($item, $src)) {
        $img->setAttribute('data-entity-type', $file->getEntityTypeId());
        $img->setAttribute('data-entity-uuid', $file->uuid());
        $img->setAttribute('src', $file->createFileUrl());
        $changed = TRUE;
      }
      elseif ($remove_broken) {
        // Remove the image if it's broken.
        $img->parentNode->replaceChild($dom->createTextNode($broken_text), $img);
        $changed = TRUE;
      }
    }
    if ($changed) {
      $this->setTextItemDom($item, $dom);
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function isValidInlineImage(\DOMElement $img): bool {
    $config = $this->getConfig();
    if ($this->isBlobImage($img->getAttribute('src')) && $config->get('ignore_blob')) {
      return TRUE;
    }
    if ($img->getAttribute('data-entity-type') !== 'file' || !$uuid = $img->getAttribute('data-entity-uuid')) {
      return FALSE;
    }
    $file_storage = $this->entityTypeManager->getStorage('file');
    $uuid_key = $file_storage->getEntityType()->getKey('uuid');
    $ids = $file_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition($uuid_key, $uuid)
      ->range(0, 1)
      ->execute();
    if (!$ids) {
      return FALSE;
    }
    if (!$this->getConfig()->get('check_file_exists')) {
      return TRUE;
    }
    // @todo Add an option to compare src with the file URI.
    //   This should be an option because in most cases the core will ensure
    //   that src is set from the file entity
    //   via the editor_file_reference filter,
    //   so the src set in the markup is not important.
    // @see \Drupal\editor\Plugin\Filter\EditorFileReference::process()
    $file = $file_storage->load(reset($ids));
    return file_exists($file->getFileUri());
  }

  /**
   * {@inheritdoc}
   */
  public function downloadInlineImage(TextItemBase $item, string $src, bool $to_temp = FALSE): ?FileInterface {
    // Skip if the image has no src attribute.
    if (!$src) {
      return NULL;
    }

    // Try to download the image.
    $file = NULL;
    try {
      if ($this->isBlobImage($src)) {
        $content = base64_decode(explode(',', $src, 2)[1] ?? '');
        $file = $this->saveImage($content, $this->getUploadDir($item, $to_temp), $this->generateFilename($content));
      }
      else {
        if (!UrlHelper::isExternal($src)) {
          $src = $this->requestContext->getCompleteBaseUrl() . $src;
        }
        $content = $this->httpClient
          ->request(Request::METHOD_GET, $src)
          ->getBody()
          ->getContents();
        $pathinfo = pathinfo(parse_url($src, PHP_URL_PATH)) + ['extension' => ''];
        if (empty($pathinfo['filename'])) {
          $pathinfo['filename'] = $this->generateFilename($content);
        }
        $file = $this->saveImage($content, $this->getUploadDir($item, $to_temp), $pathinfo['filename'], $pathinfo['extension']);
      }
    }
    catch (\Throwable) {
      // Ignore.
    }
    return $file;
  }

  /**
   * Generates a filename for the content.
   */
  protected function generateFilename(string $content): string {
    return hash('xxh64', $content);
  }

  /**
   * Checks if the src is a blob image.
   */
  protected function isBlobImage(string $src): bool {
    return str_starts_with($src, 'data:') || str_starts_with($src, 'image/');
  }

  /**
   * Saves a content as a file if it's an image.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function saveImage(string $content, string $upload_dir, string $filename, string $extension = ''): ?FileInterface {
    // @todo replace with "file.mime_type.guesser" service after https://www.drupal.org/project/drupal/issues/2388749
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime_type = finfo_buffer($finfo, $content);
    finfo_close($finfo);
    if (!str_starts_with($mime_type, 'image/')) {
      return NULL;
    }
    // Ensure the file extension matches the mime type.
    $mime_extensions = MimeTypes::getDefault()->getExtensions($mime_type);
    $mime_extension = reset($mime_extensions);
    if ($mime_extension && $mime_extension !== $extension) {
      $extension = $mime_extension;
    }
    if ($extension) {
      $filename .= ".$extension";
    }
    // Save the file.
    $uri = $this->fileSystem->saveData($content, "$upload_dir/$filename");
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->entityTypeManager->getStorage('file')->create(['uri' => $uri]);
    $file->setOwnerId($this->currentUser->id());
    $file->setFilename($filename);
    $file->setMimeType($mime_type);
    $file->setTemporary();
    $file->save();
    return $file;
  }

  /**
   * Returns the upload directory for the item.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getUploadDir(TextItemBase $item, bool $to_temp): string {
    if ($to_temp) {
      $upload_dir = 'temporary://' . static::DEFAULT_DIR_NAME;
      $this->fileSystem->prepareDirectory($upload_dir, FileSystemInterface::CREATE_DIRECTORY);
    }
    else {
      $format = $item->get('format')->getString();
      if (!isset($this->uploadDirs[$format])) {
        $upload_settings = editor_load($format)->getImageUploadSettings();
        if (!empty($upload_settings['scheme']) && !empty($upload_settings['directory'])) {
          $upload_dir = "{$upload_settings['scheme']}://{$upload_settings['directory']}";
        }
        else {
          if (!isset($this->defaultUploadDir)) {
            $default_scheme = $this->configFactory->get('system.file')->get('default_scheme');
            $this->defaultUploadDir = "$default_scheme://" . static::DEFAULT_DIR_NAME;
          }
          $upload_dir = $this->defaultUploadDir;
        }
        $this->fileSystem->prepareDirectory($upload_dir, FileSystemInterface::CREATE_DIRECTORY);
        $this->uploadDirs[$format] = $upload_dir;
      }
      $upload_dir = $this->uploadDirs[$format];
    }
    return $upload_dir;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getTextItemDom(TextItemBase $item): ?\DOMDocument {
    $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
    if (!$property_value = trim($item->get($main_property)->getString())) {
      return NULL;
    }
    if (stripos($property_value, '<img') === FALSE) {
      return NULL;
    }
    $format = $item->get('format')->getString();
    if (!$format || !$editor = editor_load($format)) {
      return NULL;
    }
    if (!$editor->getImageUploadSettings()['status'] ?? FALSE) {
      return NULL;
    }
    return Html::load($property_value);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function setTextItemDom(TextItemBase $item, \DOMDocument $value): void {
    $property_name = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
    $item->get($property_name)->setValue(Html::serialize($value));
  }

  /**
   * Returns the module config.
   */
  protected function getConfig(): ImmutableConfig {
    return $this->config ??= $this->configFactory->get(InlineImageSaverSettingsForm::CONFIG_NAME);
  }

}
