# Inline image saver

This module will help solve the problem of loss of inline images in markup from
node fields and other entities.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Configuration

1. Go to the settings form `/admin/config/media/inline-image-saver/settings`.
2. Fill out and submit the form.
